from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(unittest.TestCase):
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3)
	
	def tearDown(self):
		self.browser.quit()
	
	def test_home_and_search(self):
		self.browser.get('http://localhost:8000')
		self.assertIn('Word Bank', self.browser.title)
		
		header_text = self.browser.find_element_by_tag_name('h3').text
		self.assertIn('Word Bank', header_text)
		
		inputbox = self.browser.find_element_by_name('text')
		inputbox.send_keys('cat')
		inputbox.send_keys(Keys.ENTER)
		
		self.assertIn('Word Search', self.browser.title)
		self.assertTrue('cat')
	
	def test_word_list(self):
		self.browser.get('http://localhost:8000/list')
		try:
			table = self.browser.find_elements_by_tag_name('table')
		except:
			self.assertTrue('No word available.')
	
	def test_add_word(self):
		self.browser.get('http://localhost:8000/add')
		wordbox = self.browser.find_element_by_name('word')
		wordbox.send_keys('test')
		typebox = self.browser.find_element_by_name('word_mean')
		typebox.send_keys('test mean')
		exbox = self.browser.find_element_by_name('word_ex')
		exbox.send_keys('test ex')
		exbox.send_keys(Keys.ENTER)
		table = self.browser.find_element_by_tag_name('table')
		datas = table.find_elements_by_tag_name('td')
		self.assertTrue(
            any(data.text == 'test' for data in datas),
            "New word did not appear in table"
        )

if __name__ == '__main__':
	unittest.main(warnings='ignore')

