from django.db import models

class Word(models.Model):
	word_text = models.CharField(max_length=50)
	def __str__(self):
		return self.word_text

class Mean(models.Model):
	word = models.ForeignKey(Word,on_delete=models.CASCADE)
	word_type = models.CharField(max_length=50)
	word_mean = models.CharField(max_length=300)
	word_ex = models.CharField(max_length=300)
	def __str__(self):
		return self.word_mean

class Search(models.Model):
	search_text = models.CharField(max_length=50)
	search_time = models.DateTimeField('date published')
	search_count = models.IntegerField(default=1)
	def __str__(self):
		return self.search_text

