from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.core.files import File
from django.core.files.storage import default_storage
from django.utils import timezone
import os,csv
from xml.etree.ElementTree import Element, SubElement
from xml.etree.ElementTree import ElementTree as ET
import xml.etree.ElementTree
from lxml import etree

from .models import Word,Mean,Search

def home(request):
	time_search_list = Search.objects.order_by('-search_time')[:5]
	freq_search_list = Search.objects.order_by('-search_count')[:5]
	context = {'time_search_list':time_search_list,'freq_search_list':freq_search_list}
	return render(request,'home.html',context)

def add_word(request):
	#try-except for 1st request with empty post data
	try:
		word = request.POST['word']
		word_type = request.POST['word_type']
		word_mean = request.POST['word_mean']
		word_ex = request.POST['word_ex']
	except:
		return render(request, 'add.html')
	word_list = Word.objects.order_by('word_text')
	try:
		w = Word.objects.get(word_text=word)
	except:
		w = Word(word_text=word)
		w.save()
	w.mean_set.create(word_type=word_type,word_mean=word_mean,word_ex=word_ex)
	return HttpResponseRedirect(reverse('bank:word_list'))

def backup(request):
	csv_file = "bank/backup/backup.csv"
	word_list = Word.objects.all()
	with open(csv_file, 'w') as file:
		write = csv.writer(file, delimiter=',')
		for word in word_list:
			for mean in word.mean_set.all():
				data = [[word.word_text,mean.word_type,mean.word_mean,mean.word_ex]]
				write.writerows(data)
	return HttpResponseRedirect(reverse('bank:word_list'))

def backupXML(request):
	xml_file = "bank/backup/backup.xml"
	word_list = Word.objects.all()
	bank = Element('bank')
	for word in word_list:
		for mean in word.mean_set.all():
			w = SubElement(bank, 'word')
			te = SubElement(w, 'text')
			te.text = word.word_text
			ty = SubElement(w, 'type')
			ty.text = mean.word_type
			me = SubElement(w, 'mean')
			me.text = mean.word_mean
			ex = SubElement(w, 'ex')
			ex.text = mean.word_ex
	ET(bank).write(xml_file)
	f = open(xml_file)
	text = f.read()
	f.close()
	f = open(xml_file, 'w')
	f.write('<!DOCTYPE bank SYSTEM "std.dtd">')
	f.write(text)
	f.close()
	f = open('bank/static/backup.xml', 'w')
	f.write('<!DOCTYPE bank SYSTEM "std.dtd">')
	f.write(text)
	f.close()
	return HttpResponseRedirect(reverse('bank:word_list'))

def clear(request):
	word_list = Word.objects.all()
	for word in word_list:
		word.delete()
	return HttpResponseRedirect(reverse('bank:word_list'))

def loadCSV(request,activity):
	if activity == 'restore':
		csv_file = "bank/backup/backup.csv"
	elif activity == 'upload':
		csv_file = "bank/uploadTemp/upload.csv"
	dataReader = csv.reader(open(csv_file))
	temp_text = ''
	for row in dataReader:
		if row[0] != 'word_text':
			if row[0] != '':
				word_text = row[0]
				temp_text = row[0]
			else:
				word_text = temp_text
			word_type = row[1]
			word_mean = row[2]
			word_ex = row[3]
			try:
				w = Word.objects.get(word_text=word_text)
			except:
				w = Word(word_text=word_text)
				w.save()
			w.save()
			w.mean_set.create(word_type=word_type,word_mean=word_mean,word_ex=word_ex)
	return HttpResponseRedirect(reverse('bank:word_list'))

def loadXML(request,activity):
	if activity == 'restore':
		xml_file = "bank/backup/backup.xml"
	elif activity == 'upload':
		xml_file = "bank/uploadTemp/upload.xml"
	try:
		parser = etree.XMLParser(dtd_validation=True)
		tree = etree.parse(xml_file, parser)
	except:
		return HttpResponse('Error, Invalid XML.')
	bank = xml.etree.ElementTree.parse(xml_file)
	root = bank.getroot()
	for word in root.findall('word'):
		word_text = word.find('text').text
		word_type = word.find('type').text
		word_mean = word.find('mean').text
		word_ex = word.find('ex').text
		try:
			w = Word.objects.get(word_text=word_text)
		except:
			w = Word(word_text=word_text)
			w.save()
		w.save()
		w.mean_set.create(word_type=word_type,word_mean=word_mean,word_ex=word_ex)
	return HttpResponseRedirect(reverse('bank:word_list'))

def uploadCSV(request):
	path = 'bank/uploadTemp/'
	if request.FILES != {}:
		file = request.FILES['uploadedFile']
		filename = file.name
		if filename.endswith('.csv'):
			savePath = default_storage.save(path,file)
			os.rename(savePath,'{}upload.csv'.format(path))
			return HttpResponseRedirect(reverse('bank:loadCSV', args=('upload',)))
		elif filename.endswith('.xml'):
			savePath = default_storage.save(path,file)
			os.rename(savePath,'{}upload.xml'.format(path))
			return HttpResponseRedirect(reverse('bank:loadXML', args=('upload',)))
		else:
			return HttpResponseRedirect(reverse('bank:add_word'))
	else:
		return HttpResponseRedirect(reverse('bank:add_word'))

def word_list(request):
	word_list = Word.objects.order_by('word_text')
	context = {'word_list':word_list}
	return render(request, 'list.html', context)

def word_detail(request,word):
	word = Word.objects.get(word_text=word)
	context = {'word':word}
	return render(request, 'detail.html', context)

def word_search(request):
	text = request.POST['text']
	method = request.POST['method']
	#############################################################################################
	try:
		s = Search.objects.get(search_text=text)
		s.search_time = timezone.now()
		s.search_count += 1
		s.save()
	except:
		s = Search(search_text=text,search_time=timezone.now(),search_count=1)
		s.save()
	#############################################################################################
	if method == 'startswith':
		word_list = Word.objects.filter(word_text__startswith=text)
	elif method == 'endswith':
		word_list = Word.objects.filter(word_text__endswith=text)
	else:
		word_list = Word.objects.filter(word_text__contains=text)
	context = {'text':text,'method':method,'word_list':word_list}
	return render(request, 'search.html', context)

def lastest_search(request):
	search_list = Search.objects.order_by('-search_time')[:]
	context = {'search_list':search_list}
	return render(request, 'lastest.html', context)

def freq_search(request):
	search_list = Search.objects.order_by('-search_count')[:]
	context = {'search_list':search_list}
	return render(request, 'freq.html', context)

