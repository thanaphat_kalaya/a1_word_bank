# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-03-01 06:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bank', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mean',
            name='word_type',
            field=models.CharField(default='-', max_length=50),
            preserve_default=False,
        ),
    ]
