from django.conf.urls import url
from . import views

app_name = 'bank'
urlpatterns = [
	url(r'^$', views.home, name='home'),
	url(r'^add$', views.add_word, name='add_word'),
	url(r'^backup$', views.backup, name='backup'),
	url(r'^backupXML$', views.backupXML, name='backupXML'),
	url(r'^upload$', views.uploadCSV, name='uploadCSV'),
	url(r'^loadCSV/(?P<activity>\w+)$', views.loadCSV, name='loadCSV'),
	url(r'^loadXML/(?P<activity>\w+)$', views.loadXML, name='loadXML'),
	url(r'^clear$', views.clear, name='clear'),
	url(r'^list$', views.word_list, name='word_list'),
	url(r'^lastest$', views.lastest_search, name='lastest_search'),
	url(r'^freq$', views.freq_search, name='freq_search'),
	url(r'^detail/(?P<word>\w+)$', views.word_detail, name='word_detail'),
	url(r'^search$', views.word_search, name='word_search')
]
