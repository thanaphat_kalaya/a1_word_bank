from django.contrib import admin
from .models import Word,Mean,Search

class Mean_Inline(admin.TabularInline):
	model = Mean
	extra = 1
	fieldsets = [
		(None,{'fields': ['word_type']}),
		('Meaning',{'fields': ['word_mean']}),
		('Examples',{'fields': ['word_ex']}),
	]
	list_display = ('word_type','word_mean','word_ex')

class Word_Mean(admin.ModelAdmin):
	inlines = [Mean_Inline]

admin.site.register(Word,Word_Mean)
admin.site.register(Search)
